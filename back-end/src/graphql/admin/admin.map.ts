
export const AdminMap = {
    jwt: (administrator) => administrator.getJwt(),
    abilities: (administrator) => administrator.getAbilities()
};