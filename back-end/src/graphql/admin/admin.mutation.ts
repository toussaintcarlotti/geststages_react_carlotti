import { resolver as rs } from 'graphql-sequelize';
import { admin } from "../../models";
import to from 'await-to-js';
import sequelize = require('sequelize');

export const Mutation = {
    createAdmin: rs(admin, {
      before: async (findOptions, { data }) => {
        let err, administrator;
        console.log(JSON.stringify(data));
        [err, administrator] = await to(admin.create(data) );
        if (err) {
          throw err;
        }
        findOptions.where = { num_admin:administrator.num_admin };
        return findOptions;
      },
      after: (administrator) => {
        administrator.connect = true;
        return administrator;
      }
    }),

    updateAdmin: rs(admin, {
      before: async (findOptions, { data }) => {
        let err, administrator;
        console.log(JSON.stringify(data));
        const update: sequelize.UpdateOptions = {
          where: { num_admin: data.num_admin },
          limit: 1
        };
        [err, administrator] = await to(admin.update(update, data));
        if (err) {
          throw err;
        }
        findOptions.where = { num_admin: data.num_admin };
        return findOptions;
      },
      after: (administrator) => {
        administrator.connect = true;
        return administrator;
      }
    })
};