import { resolver } from 'graphql-sequelize';
import {admin, professeur} from "../../models";
import to from 'await-to-js';

export const Query = {
    getAdmin: resolver(admin, {
        before: (findOptions, {}, {user}) => {
            findOptions.where = {num_admin: user.num_admin};
            return findOptions;
        },
        after: (administrator) => {
            return administrator;
        }
    }),
    loginAdmin: resolver(admin, {
        before: (findOptions, { login }) => {
            findOptions.where = {login};
            return findOptions;
        },
        after: async (administrator, { password }) => {
            let err;
            [err, administrator] = await to(administrator.comparePassword(password));
            if(err) {
              console.log(err);
              throw new Error(err);
            }

            administrator.connect = true;//to let the directive know to that this user is authenticated without an authorization header
            return administrator;
        }
    }),
    logoutAdmin: resolver(admin, {
        after: async (administrator) => {
            console.log(administrator);
            return administrator;
        }
    }),
    getAllAdmins: resolver(admin),
    getAdminByName: resolver(admin, {
        after: (findOptions, { nom_admin }) => {
            findOptions.where = {nom_admin};
            return findOptions;
        }
    })
};