import { Table, Column, Model, BeforeSave } from 'sequelize-typescript';
import * as bcrypt from 'bcrypt';
import to from 'await-to-js';
import * as jsonwebtoken from'jsonwebtoken';
import { ENV } from '../config';

@Table({timestamps: false})
export class admin extends Model<admin> {

    @Column({primaryKey: true, autoIncrement: true})
    num_admin: number;

    @Column({defaultValue: ""})
    nom_admin: string;

    @Column({defaultValue: ""})
    prenom_admin: string;

    @Column({unique: true})
    login: string;

    @Column({defaultValue: ""})
    mdp: string;


    jwt: string;
    abilities:string;
    connect: boolean;
    @BeforeSave
    static async hashPassword(admin: admin) {
        let err;
        if (admin.changed('mdp')){
            let salt, hash;
            [err, salt] = await to(bcrypt.genSalt(10));
            if(err) {
                throw err;
            }

            [err, hash] = await to(bcrypt.hash(admin.mdp, salt));
            if(err) {
                throw err;
            }
            admin.mdp = hash;
        }
    }

    async comparePassword(pw) {
        let err, pass;
        if(!this.mdp) {
            throw new Error('Does not have password');
        }

        [err, pass] = await to(bcrypt.compare(pw, this.mdp));
        if(err) {
            throw err;
        }

        if(!pass) {
            throw 'Invalid password';
        }

        return this;
    }


    getJwt(){
        return 'Bearer ' + jsonwebtoken.sign({
            num_admin: this.num_admin,
        }, ENV.JWT_ENCRYPTION, { expiresIn: ENV.JWT_EXPIRATION });
    }

    

    getAbilities(){
        return '***';
    }
}