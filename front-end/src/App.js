import React /*, { useState }*/, {useState} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

/*
 * Authentification
 */
import PrivateRoute from "./Routes/PrivateRoute";
import RouteAbilitie2 from "./Routes/RouteAbilitie2";
import RouteAbilitie3 from "./Routes/RouteAbilitie3";


/*
 * Pages
 */

import Home from './pages/Home/Home';
import Login from './pages/Login/Login';
/*
 * Entreprise
 */

import Entreprise from "./pages/Entreprise/Entreprise";
import AjoutEntreprise from "./pages/Entreprise/AjoutEntreprise";
import RechercheEntreprise from "./pages/Entreprise/RechercheEntreprise";


/*
 * Stagiaires
 */

import Stagiaires from "./pages/Stagiaires/Stagiaires";

/*
 * Apollo Client
 */
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import Sidebar from "./components/Sidebar/Sidebar";
import AjoutStagiaire from "./pages/Stagiaires/AjoutStagiaire";
import Navigation from "./components/Navigation/Navigation";

const client = new ApolloClient({
    uri: "http://localhost:3001/graphql",
    request: async operation => {

        // Get JWT token from local storage
        const token = window.localStorage.getItem('token')
        // Pass token to headers
        operation.setContext({
            headers: {
                Authorization: token ? `${token}` : ''
            }
        })
    }
});
let sidebar, navbar;


function App(props) {
        //const [token, setToken] = useState(window.localStorage.getItem('token')) ;

        //setToken(window.localStorage.getItem('token'));

        /*const isLoggedIn = props.isLoggedIn;
        if (isLoggedIn)
            return <Sidebar />;
        else
            return <div></div>;

         */


    return (
        <ApolloProvider client = { client }>
            <Router>
                <Switch>
                    {/*
                        Routes
                        PrivateRoute : Authentification requise / permission
                        Route : Route publique utilisée par defaut
                         */}
                    <Route path="/login" component = { Login } />
                    <PrivateRoute path="/" exact component = { Home } />

                    <PrivateRoute path="/entreprises" exact component = { Entreprise } />
                    <PrivateRoute path="/entreprises/ajout" component = { AjoutEntreprise } />
                    <PrivateRoute path="/entreprises/recherche" component = { RechercheEntreprise } />

                    <PrivateRoute path="/stagiaires" exact component = { Stagiaires } />
                    <PrivateRoute path="/stagiaires/ajout" component = { AjoutStagiaire } />


                </Switch>

            </Router>

        </ApolloProvider>
    );
}
export default App;