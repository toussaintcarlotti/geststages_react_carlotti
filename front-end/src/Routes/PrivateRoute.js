import React from "react";
import { Route, Redirect } from "react-router-dom";



function PrivateRoute({ component: Component, ...rest }) {

    //Vérification de l'authentification
    const token = window.localStorage.getItem('token')

    return (
        //Si l'utilisateur est authentifié alors on affiche le composant
        // à l'inverse on le redirige vers la connexion
        <
        Route {...rest }
        render = {
            props => token ? ( <Component {...props }/> ) : <Redirect to= "login" />
            }
            />
        );
    }
    export default PrivateRoute;