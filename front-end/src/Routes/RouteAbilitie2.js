import React from "react";
import { Route, Redirect } from "react-router-dom";

function RouteAbilitie2({ component: Component, ...rest }) {
    //Vérification de l'authentification
    const token = window.localStorage.getItem('token')
    var abilities;
    if (window.localStorage.getItem('abilities') === "**" || window.localStorage.getItem('abilities') === "***" ){
        abilities = window.localStorage.getItem('abilities');
    }
    return (
        //Si l'utilisateur est authentifié alors on affiche le composant
        // à l'inverse on le redirige vers la connexion
        <
            Route {...rest }
                  render = {
                      props => token && abilities ? ( <Component {...props }/> ) : <Redirect to= "/" />
                  }
        />
    );
}
export default RouteAbilitie2;