import React from "react";
import { Route, Redirect } from "react-router-dom";

function RouteAbilitie3({ component: Component, ...rest }) {


    const token = window.localStorage.getItem('token')
    var abilities;
    if (window.localStorage.getItem('abilities') === "***"){
        abilities = window.localStorage.getItem('abilities');
    }
    //Vérification de l'authentification
    return (
        //Si l'utilisateur est authentifié alors on affiche le composant
        // à l'inverse on le redirige vers la connexion
        <
            Route {...rest }
                  render = {
                      props => token && abilities ? ( <Component {...props }/> ) : <Redirect to= "/" />
                  }
        />
    );
}
export default RouteAbilitie3;