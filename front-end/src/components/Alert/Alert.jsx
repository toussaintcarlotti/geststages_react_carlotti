import classNames from 'classnames';
import React, { Component } from 'react';

import './Alert.css';

class Alert extends Component {
    render() {
        const classes = classNames('alert', `alert-${this.props.type}`, 'shadow');
        return (
            <div className={classes} role="alert">
                {this.props.content}
            </div>
        );
    }
}
export default Alert;