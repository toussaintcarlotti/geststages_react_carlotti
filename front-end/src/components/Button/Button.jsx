import classNames from 'classnames';
import React, { Component } from 'react';

import './Button.css';

class Button extends Component {
    render() {
        const classes = classNames('btn', `btn-${this.props.color}`);
        return (<button className={classes}>{this.props.value}</button>);
    }
}
export default Button;