import React, { Component } from 'react';

import './Navigation.css';
import {Link} from "react-router-dom";



class Navigation extends Component {
    logout() {
        window.localStorage.clear();
    }
    toggle(){
        if (document.getElementById("collapseEx2") === 'true'){
            console.log('sdfc')

        }
    }
    render() {
        return (
            <nav className="navbar navCustom navbar-dark">

                <button onClick={this.toggle} className="navbar-toggler hidden-sm-up float-right" type="button" data-toggle="collapse" data-target="#collapseEx2">
                    <i className="fa fa-bars"></i></button>

                <div className="container ">
                    <div className="header">
                        <Link className="text-white text-decoration-none" to="/" ><span>Gestion des stages</span></Link>
                    </div>
                    <div className="collapse navbar-toggleable-xs row " id="collapseEx2">
                        <ul className="nav navbar-nav mx-5 ">
                            <h2 className="navBar-titleCustom">Général</h2>
                            <li className="nav-item ">
                                <Link to="/entreprises" className="nav-link">Entreprises</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/stagiaires" className="nav-link">Stagiaires</Link>
                            </li>
                        </ul>
                        <ul className="nav navbar-nav mx-5 ">
                            <h2 className="navBar-titleCustom">Compte</h2>
                            <li className="nav-item ">
                                <Link to="/" onClick={this.logout} className="nav-link">Déconnexion</Link>
                            </li>
                        </ul>
                    </div>

                </div>

            </nav>
        );
    }
}
export default Navigation;