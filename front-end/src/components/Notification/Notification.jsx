import React, { Component } from 'react';
import Noty from 'noty';

class Notification extends Component {
    render() {
        return new Noty({
            type: this.props.type,
            theme: 'nest',
            text: this.props.value
        }).show();
    }
}
export default Notification;