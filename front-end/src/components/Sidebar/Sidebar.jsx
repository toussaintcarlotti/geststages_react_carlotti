import React, { Component } from 'react';

import './Sidebar.css';
import {Link} from "react-router-dom";

class Sidebar extends Component {
    logout() {
        window.localStorage.clear();
    }
    render() {
        return (
            <nav className="sidebar">
                <div className="sidebar-header">
                    <Link className="text-white text-decoration-none" to="/" ><span>Gestion des stages</span></Link>
                </div>
                <div className="sidebar-content">

                    <ul className="list-unstyled components">
                        <h2 className="sidebar-title">Général</h2>
                        <li>
                            <Link to="/entreprises" className="nav-link">Entreprises</Link>
                        </li>
                        <li>
                            <Link to="/stagiaires" className="nav-link">Stagiaires</Link>
                        </li>
                    </ul>
                    <ul className="list-unstyled components">
                        <h2 className="sidebar-title">Compte</h2>
                        <li>
                            <Link to="/" onClick={this.logout} className="nav-link">Déconnexion</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Sidebar;