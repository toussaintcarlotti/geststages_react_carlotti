import React, { Component } from 'react';

import Sidebar from "../../components/Sidebar/Sidebar";
import Navigation from "../../components/Navigation/Navigation";

import './Default.css';
import {Route} from "react-router-dom";

/*
 * Template de base pour les pages
 * Etant donné que la page de Connexion est totalement différente
 * Celle-ci ne dépend pas de ce template
*/
class Default extends Component {
    render() {
        return (
            <div className="wrapper">
                <Sidebar />
                <div className="main">
                    <Navigation />
                    <div className="content">
                        <div className="container-fluid">
                            { this.renderContent() }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    /*
     * Renvoie le contenu de la page
     * Cela nous evite de dupliquer dans chacune des pages
     * les composants essentials, Sidebar, Navigation
     */
    renderContent() {}
}

export default Default;