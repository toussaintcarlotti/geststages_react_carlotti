import React, {useState} from 'react';

import {
    FormControl,
    Container,
    TextField,
    Card,
    CardContent,
    Grid,
    MenuItem,
    Button,
    withStyles,
    Box
} from '@material-ui/core';

import {CREATE_ENTREPRISE} from "./queries";

import Default from "../Default/Default";
import * as PropTypes from "prop-types";

import { Mutation } from 'react-apollo';

const styles = theme => ({

    card: {
        marginBottom: 10,
        textAlign: 'center',
        backgroundColor: '#0284aa',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    texta: {
        fontSize: 15,
        textAlign: 'right'
    },
    bouton: {
        backgroundColor: '#0284aa',
        marginBottom: 50,
        fontWeight: 'bold',
        color: 'white'
    }
});

const currencies = [
    {
        value: 'SLAM',
        label: 'SLAM',
    },
    {
        value: 'SISR',
        label: 'SISR',
    },
    {
        value: 'ECO',
        label: 'ECO',
    },
    {
        value: 'SN',
        label: 'SN',
    },
];



export class AjoutEntreprise extends Default{
        constructor() {
            super();
            this.state = {
                raison_sociale: '',
                nom_contact: '',
                nom_resp: '',
                rue_entreprise: '',
                cp_entreprise: '',
                ville_entreprise: '',
                tel_entreprise: '',
                fax_entreprise: '',
                email: '',
                observation: '',
                site_entreprise: '',
                niveau: '',
                en_activite: ''
            }
        };

    handleInputChange = (event) => {
        event.persist();
        this.setState(values => ({
            ...values,
            [event.target.name]: event.target.value
        }))
    }
    handleSubmit = () => {
        console.log(this.state);
    }

    renderContent() {

        const { classes } = this.props;

        // FONCTION POUR CHOISIR UNE SPECIALITE

/*
        const ChangeChoice = (event) => {
            this.setState({currency: event.target.value});
        };

 */

        return (

            <Mutation mutation={CREATE_ENTREPRISE} variables={{entreprise: this.state}}>
                {(createEntreprise, { data, loading, error })=>(
                    <form onSubmit={async e => {
                        this.state.cp_entreprise = parseInt(this.state.cp_entreprise);
                        this.state.en_activite = parseInt(this.state.en_activite);
                        e.preventDefault();
                        const response = await createEntreprise();
                        console.log(response);
                    }}>
                        {/* FORMULAIRE INFORMATION */}

                        <Box justifyContent="center" container spacing={3} justify="flex-end" id="information">

                            <Grid item xs={11}>
                                <Card className={classes.card}>
                                    <CardContent className={classes.text}>Information</CardContent>
                                </Card>
                            </Grid>

                            <Grid item xs={4}>
                                <CardContent className={classes.texta}>Nom de l'entreprise* : </CardContent>
                            </Grid>
                            <Grid item xs={8}>
                                <FormControl fullWidth
                                             onChange={this.handleInputChange}
                                >
                                    <TextField style={{marginBottom: 20}}
                                               required
                                               id="outlined-required"
                                               variant="outlined"
                                               name='raison_sociale'
                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={4}>
                                <CardContent className={classes.texta}>Nom du contact : </CardContent>
                            </Grid>
                            <Grid item xs={8}>
                                <FormControl fullWidth
                                             onChange={this.handleInputChange}>
                                    <TextField style={{marginBottom: 20}}
                                               id="outlined-required"
                                               variant="outlined"
                                               name="nom_contact"

                                    />
                                </FormControl>
                            </Grid>

                            <Grid item xs={3}>
                                <CardContent className={classes.texta}>Nom du responsable : </CardContent>
                            </Grid>
                            <Grid item xs={8}>
                                <FormControl fullWidth
                                             onChange={this.handleInputChange}
                                >
                                    <TextField style={{marginBottom: 20}}
                                               id="outlined-required"
                                               variant="outlined"
                                               name="nom_resp"

                                    />
                                </FormControl>
                            </Grid>




                                <Card className={classes.card}>
                                    <CardContent className={classes.text}>Contact</CardContent>
                                </Card>



                                <FormControl fullWidth>
                                    <TextField style={{marginBottom: 20}}
                                               required
                                               id="outlined-required"
                                               variant="outlined"
                                               name="rue_entreprise"
                                               label="Rue"
                                               onChange={this.handleInputChange}/>
                                </FormControl>

                                <FormControl fullWidth>
                                    <TextField style={{marginBottom: 20}}
                                               required
                                               id="outlined-required"
                                               variant="outlined"
                                               name="cp_entreprise"
                                               label="Code postal"
                                               onChange={this.handleInputChange}
                                    />
                                </FormControl>

                                <FormControl fullWidth>
                                    <TextField style={{marginBottom: 20}}
                                               required
                                               id="outlined-required"
                                               variant="outlined"
                                               name="en_activite"
                                               label="En activite"
                                               onChange={this.handleInputChange}
                                    />
                                </FormControl>

                            <Button color="default" variant="outlined" type="submit">Ajouter</Button>
                        </Box>
                    </form>
                )}

            </Mutation>
        );
    }
}
export default  withStyles(styles)(AjoutEntreprise);
