import React, { useState } from 'react';
import { Link } from "react-router-dom";

import './Entreprise.css';

import { Query, useQuery } from 'react-apollo';

import { GET_ENTREPRISE } from './queries'
import Alert from "../../components/Alert/Alert";

import { Button, TextField } from '@material-ui/core';
import Default from "../Default/Default";

import { Form } from "react-bootstrap";
import Sidebar from "../../components/Sidebar/Sidebar";
import Navigation from "../../components/Navigation/Navigation";

function Entreprise() {
    const [entreprises, setEntreprises] = useState([]);
    let { loading, error, data } = useQuery(GET_ENTREPRISE);
    if (loading){
        console.log(loading);
    }
    
    const handleSearch = (e) => {
        const testTab = [];
        e.persist();
        console.log('etarget: ' + e.target.value)
        data.getEntreprise.forEach(business => {
            if (business.raison_sociale.toLowerCase().includes(e.target.value)) {
                testTab.push(business);
            }
        });
        setEntreprises(testTab);
    }
    if (data && entreprises.length === 0) {
        setEntreprises(data.getEntreprise.slice())
    }
    return (

        <div>
            <Sidebar />
            <Navigation />
            <div className="content">


                {/* BOUTON POUR AJOUTER UNE ENTREPRISE */}
                <Link to="entreprises/ajout" className="nav-link">
                    <Button
                        variant="contained"
                        color="inherit"
                        style={{ backgroundColor: 'darkgrey', color: 'black', marginLeft: 20 }}
                    >
                        Ajouter une entreprise
                    </Button>
                </Link>

                {/* BOUTON POUR RECHERCHER UNE ENTREPRISE */}
                <Form className="ml-4">
                    {<TextField onChange={handleSearch}
                        label="Rechercher"
                        name="inputSearch"
                    >
                    </TextField>}
                </Form>

                <div className="col-md-12" >
                    <div className="content-card shadow">
                        <h3 className="content-title">Liste des entreprises</h3>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">Entreprise</th>
                                    <th scope="col">Adresse</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Site</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    entreprises.map(business => (
                                        <tr key={business.num_entreprise}>
                                            <td>{business.raison_sociale}</td>
                                            <td>{business.rue_entreprise + ", " + business.cp_entreprise + ", " + business.ville_entreprise}</td>
                                            <td>{business.email}</td>
                                            <td>{business.site_entreprise}</td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Entreprise;