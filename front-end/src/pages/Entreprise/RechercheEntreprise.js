import React from 'react';

import {FormControl, Container, TextField, Card, CardContent, Grid, MenuItem, Button} from '@material-ui/core';

import { withStyles } from "@material-ui/core";
import Default from "../Default/Default";

const styles = theme => ({
    body: {
      marginTop: 75,
    },
    card: {
      padding: theme.spacing(0),
      textAlign: 'center',
      backgroundColor: '#0284aa',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    texta: {
        fontSize: 15,
        textAlign: 'right'
    },
    bouton: {
        backgroundColor: '#0284aa',
        marginBottom: 50,
        fontWeight: 'bold',
        color: 'white'
    }
});

const specialites = [
    {
      value: ' ',
      label: ' ',
    },
    {
      value: 'SLAM',
      label: 'SLAM',
    },
    {
      value: 'SISR',
      label: 'SISR',
    },
    {
      value: 'ECO',
      label: 'ECO',
    },
    {
      value: 'SN',
      label: 'SN',
    },
  ];

const ville = [
    {
      value: ' ',
      label: ' ',
    },
    {
      value: 'ajaccio',
      label: 'ajaccio',
    },
    {
      value: 'bastia',
      label: 'bastia',
    },
    {
      value: 'calvi',
      label: 'calvi',
    },
    {
      value: 'sarrola carcopino',
      label: 'sarrola carcopino',
    },
    {
      value: 'sarrola-carcopino',
      label: 'sarrola-carcopino',
    },
    {
      value: 'cuttoli-corticchiato',
      label: 'cuttoli-corticchiato',
    },
  ];

class RechercheEntreprise extends Default {
    constructor(props) {
        super(props);
        this.state = {
            currencyS: '',
            currencyV: '',



            raison_sociale: '',
            nom_contact: '',
            nom_resp: '',
            rue_entreprise: '',
            cp_entreprise: '',
            ville_entreprise: '',
            tel_entrprise: '',
            fax_entreprise: '',
            email: '',
            observation: '',
            site_entreprise: '',
            niveau: '',
            en_activite: '',
            spec_entreprises: '',
            currency: ''
        };
    }

    renderContent() {
        const {classes} = this.props;
        // FONCTION POUR CHOISIR UNE SPECIALITE ET UNE VILLE

        const handleChangeSpecialite = (event) => {
            this.setState({ currencyS: event.target.value });
        };

        const handleChangeVille = (event) => {
            this.setState({ currencyV: event.target.value });
        };

        return (
            <Container className={classes.body}>

                {/* FORMULAIRE RECHERCHE */}

                <Grid container spacing={3} justify="flex-end" id="recherche">

                    <Grid item xs={11}>
                        <Card className={classes.card}>
                            <CardContent className={classes.text}>Recherche</CardContent>
                        </Card>
                    </Grid>

                    <Grid item xs={4}>
                        <CardContent className={classes.texta}>Ville : </CardContent>
                    </Grid>
                    <Grid item xs={8}>
                        <FormControl fullWidth>
                            <TextField style={{marginBottom: 20}}
                                       id="outlined-required"
                                       select
                                       variant="outlined"
                                       fullWidth
                                       value={this.state.currencyV}
                                       onChange={handleChangeVille}
                            >
                                {ville.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </FormControl>
                    </Grid>

                    <Grid item xs={4}>
                        <CardContent className={classes.texta}>Nom : </CardContent>
                    </Grid>
                    <Grid item xs={8}>
                        <FormControl fullWidth>
                            <TextField style={{marginBottom: 20}}
                                       id="outlined-required"
                                       variant="outlined"
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={4}>
                        <CardContent className={classes.texta}>Spécialité : </CardContent>
                    </Grid>
                    <Grid item xs={8}>
                        <FormControl fullWidth>
                            <TextField style={{marginBottom: 20}}
                                       id="outlined-required"
                                       select
                                       variant="outlined"
                                       fullWidth
                                       value={this.state.currencyS}
                                       onChange={handleChangeSpecialite}
                            >
                                {specialites.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </FormControl>
                    </Grid>


                    <Button style={{backgroundColor: '#0284AA', color: 'white'}} variant="outlined" size="large"
                            className="bouton">Rechercher</Button>

                </Grid>

            </Container>
        );
    }
}

export default withStyles(styles)(RechercheEntreprise);
