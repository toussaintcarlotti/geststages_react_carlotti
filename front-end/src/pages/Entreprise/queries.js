import { gql} from "apollo-boost";
import { useMutation} from "@apollo/react-hooks";

const GET_ENTREPRISE = gql`
  {
    getEntreprise {
      num_entreprise
      raison_sociale
      rue_entreprise
      cp_entreprise
      ville_entreprise
      fax_entreprise
      email
      site_entreprise
    }
  }
`;

const CREATE_ENTREPRISE = gql`
mutation($entreprise: EntrepriseInput!){
      createEntreprise(data: $entreprise) { num_entreprise }
}
`;


export {
    GET_ENTREPRISE,
    CREATE_ENTREPRISE
}