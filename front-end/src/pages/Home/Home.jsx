import React from 'react';
import { GET_ADMIN } from "../Stagiaires/queries";
import Default from "../Default/Default";
import './Home.css';
import Alert from "../../components/Alert/Alert";

import { Query } from 'react-apollo';
import {FormControl, InputLabel, MenuItem, Select} from "@material-ui/core";

class Home extends Default {
    renderContent() {
        function click(){

        }
        return (
            <div className="row">
                <div className="col-md-6">
                    <div className="content-card shadow">
                        <h3 className="content-title">Titre d'un bloc</h3>
                        <Alert
                            type="primary"
                            content="Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum"
                        />
                        <p>pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora. Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora.</p>
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="content-card shadow">
                        <h3>Titre d'un bloc</h3>
                        <p>Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora. Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora. Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora.</p>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="content-card shadow">
                        <h3>Titre d'un bloc</h3>
                        <p>Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora. Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora. Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora.</p>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="content-card shadow">
                        <h3>Titre d'un bloc</h3>
                        <Alert
                            type="danger"
                            content="Eodem tempore etiam Hymetii viri negotium est actitatum"
                        />
                        <p>Ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora. Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora. Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora.</p>
                    </div>
                </div>

                <div className="col-md-4">
                    <div className="content-card shadow">
                        <h3>Titre d'un bloc</h3>
                        <p>Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora. Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora. Eodem tempore etiam Hymetii praeclarae indolis viri negotium est actitatum, cuius hunc novimus esse textum. cum Africam pro consule regeret Carthaginiensibus victus inopia iam lassatis, ex horreis Romano populo destinatis frumentum dedit, pauloque postea cum provenisset segetum copia, integre sine ulla restituit mora.</p>
                    </div>
                </div>
            </div>
        );
    }
}
export default Home;