import React, {Component, useState} from 'react'
import { useLazyQuery } from '@apollo/react-hooks'
import { Redirect } from 'react-router'
import {LOGIN_ADMIN, LOGIN_PROF, LOGIN_ETU} from './queries'
import { useForm } from './hooks'
import { Form } from "react-bootstrap";
import './Login.css';
import Button from "../../components/Button/Button";
import Alert from "../../components/Alert/Alert";

const Login = (props) => {

        const { values, handleChange, handleSubmit } = useForm((credentials) => login(), {
            login: '',
            password: '',
            radio: null
        });

        let loadingGif,log,errorMess,querie;
        if (props){
            errorMess = props.errorMess;
        }

        if (values.radio === "admin") {
            querie = LOGIN_ADMIN;
            log = 'loginAdmin';
        }
        if (values.radio === "prof") {
            querie = LOGIN_PROF;
            log = 'loginProfesseur';
        }
        if (values.radio === "etu") {
            querie = LOGIN_ETU;
            log = 'loginEtudiant';
        }

        const [login, {called, loading, data, error}] = useLazyQuery(querie, {variables: values});

        if (called && loading) {
            loadingGif = <img className="loading" src={process.env.PUBLIC_URL + '/loading.gif'} alt="loading"/>
        }
        // Show error message if lazy query fails
        if (error) {
            if (!error.networkError) {
                errorMess = <Alert content="Nom d'utilisateur ou mot de passe incorrect " type="danger"/>;
            } else {
                errorMess = <Alert content={error.message} type="danger"/>;
            }
            return <Login errorMess={errorMess}/>
        }

        // Store token if login is successful
        if (data) {
            window.localStorage.setItem('token', data[log].jwt);
            window.localStorage.setItem('abilities', data[log].abilities);
            // document.location.reload();
            // Redirect to home page

            return <Redirect to={{
                path: '/',
                state: { reload: true }
            }}/>

        }

        return (
            <div className="login-card shadow text-center">
                {errorMess}
                    <h1 className="mb-4 title">Connexion</h1>
                    <p className="mb-4">Bienvenue sur l'application de gestion des stages du lycée <strong>Laetitia
                        Bonaparte</strong>!</p>
                    {/*
                    <Alert
                        type="primary"
                        content="Cette espace est reservée aux étudiants et professeurs dans le cadre des stages effectués dans le suppérieur."/>
                    */}

                <Form onSubmit={handleSubmit}>
                    <Form.Row>
                        <Form.Control
                            type="text"
                            id='login'
                            name='login'
                            className="login-input"
                            placeholder="Nom d'utilisateur"
                            required
                            value={values.login}
                            onChange={handleChange}/>

                        <Form.Control
                            type="password"
                            className="login-input"
                            placeholder="Mot de passe"
                            id='password'
                            name='password'
                            required
                            value={values.password}
                            onChange={handleChange}/>
                    </Form.Row>
                    <Form.Row>
                        <Form.Check required value="admin" checked={values.radio === "admin"} onChange={handleChange}
                                    type="radio" name="radio" id="admin" label="Administrateur"/>
                        <Form.Check required value="prof" checked={values.radio === "prof"} onChange={handleChange}
                                    type="radio" name="radio" id="prof" label="Professeur"/>
                        <Form.Check required value="etu" checked={values.radio === "etu"} onChange={handleChange}
                                    type="radio" name="radio" id="etu" label="Etudiant"/>
                    </Form.Row>
                    <Button
                        type="submit"
                        color="primary"
                        value="Connexion"
                    />
                    <div>{loadingGif}</div>
                </Form>
            </div>
        )
}

export default Login;