import { gql } from "apollo-boost";

const LOGIN_ADMIN = gql `
query loginAdmin($login: String!, $password: String!) {
    loginAdmin(login: $login, password: $password) {
        num_admin,
        jwt,
        abilities
    }
}
`;

const LOGIN_PROF = gql `
query loginProfesseur($login: String!, $password: String!) {
    loginProfesseur(login: $login, password: $password) {
        num_prof,
        jwt,
        abilities
    }
}
`;

const LOGIN_ETU = gql `
query loginEtudiant($login: String!, $password: String!) {
    loginEtudiant(login: $login, password: $password) {
        num_etudiant,
        jwt,
        abilities
    }
}
`;


export {
    LOGIN_ADMIN,
    LOGIN_ETU,
    LOGIN_PROF
}