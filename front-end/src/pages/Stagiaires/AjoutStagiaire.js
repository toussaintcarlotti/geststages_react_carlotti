import React, {useState} from 'react';
import { gql} from "apollo-boost";
import { Query } from 'react-apollo';
import { CREATE_STUDENT, GET_ALL_CLASSES } from "./queries";


import {
    FormControl,
    Container,
    TextField,
    Card,
    CardContent,
    Grid,
    MenuItem,
    Button,
    withStyles,
    Box, InputLabel, Select
} from '@material-ui/core';


import Default from "../Default/Default";
import * as PropTypes from "prop-types";

import { Mutation } from 'react-apollo';
import {GET_ENTREPRISE} from "../Entreprise/queries";
import Alert from "../../components/Alert/Alert";

const styles = theme => ({
    body: {
        alignContent: 'center'
    },
    card: {
        textAlign: 'center',
        backgroundColor: '#0284aa',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    texta: {
        fontSize: 15,
        textAlign: 'right'
    },
    bouton: {
        backgroundColor: '#0284aa',
        fontWeight: 'bold',
        color: 'white'
    },
    select: {
        width: '95%',
        marginTop: theme.spacing(3)
    },
    form: {
        '& .MuiTextField-root': {
            marginTop: theme.spacing(3),
            width: '95%'
        },
        '& .MuiButton-root':{
            marginTop: theme.spacing(3),
        }
    }
});

const currencies = [
    {
        value: 'SLAM',
        label: 'SLAM',
    },
    {
        value: 'SISR',
        label: 'SISR',
    },
    {
        value: 'ECO',
        label: 'ECO',
    },
    {
        value: 'SN',
        label: 'SN',
    },
];



export class AjoutStagiaire extends Default{
    constructor() {
        super();
        this.state = {
            nom_etudiant : '',
            prenom_etudiant: '',
            login : '',
            mdp : '',
            num_classe: ''
        }
    };

    handleInputChange = (event) => {
        event.persist();
        this.setState(values => ({
            ...values,
            [event.target.name]: event.target.value
        }))
    }

    renderContent() {
        const { classes } = this.props;
        return (
            <Mutation
                justifyContent="center"
                className={classes.body}
                mutation={CREATE_STUDENT}
                variables={{stagiaire: this.state}}>
                {(createStagiaire, { data, loading, error })=>(

                    <Box display="flex" justifyContent="center" m={1} p={1} bgcolor="background.paper">
                       <form
                           className={classes.form}
                           onSubmit={async e => {
                               this.state.num_classe = parseInt(this.state.num_classe);
                               e.preventDefault();
                               const response = await createStagiaire();
                               console.log(response);
                           }}>

                           <Card className={classes.card}>
                               <CardContent className={classes.text}>Information</CardContent>
                           </Card>

                           <FormControl fullWidth>
                               <TextField   id="outlined-required"
                                           required
                                           variant="outlined"
                                           name="nom_etudiant"
                                           label="Nom"
                                           onChange={this.handleInputChange}
                               />
                           </FormControl>

                           <FormControl fullWidth>
                               <TextField id="outlined-required"
                                          required
                                          variant="outlined"
                                          name="prenom_etudiant"
                                          label="Prenom"
                                          onChange={this.handleInputChange}/>
                           </FormControl>

                           <FormControl fullWidth>
                               <TextField id="outlined-required"
                                          required
                                          variant="outlined"
                                          name="login"
                                          label="Login"
                                          onChange={this.handleInputChange}/>
                           </FormControl>

                           <FormControl fullWidth >
                               <TextField id="outlined-required"
                                          required
                                          variant="outlined"
                                          name="mdp"
                                          label="Mot de passe"
                                          onChange={this.handleInputChange}
                               />
                           </FormControl>

                           <Query query={GET_ALL_CLASSES}>

                               {({loading, error, data}) => {
                                   if (loading) {
                                       return <img className="loading" src={process.env.PUBLIC_URL + '/loading.gif'}
                                                   alt="loading"/>;
                                   }
                                   if (error) {
                                       console.log('erreur');
                                       return <Alert type="danger" content={error.message}/>;
                                   }
                                   return (
                                       <FormControl variant="outlined" className={classes.select} required>
                                           <InputLabel id="demo-simple-select-outlined-label">Classe</InputLabel>
                                           <Select
                                               id="demo-simple-select-outlined-label"

                                               type="number"
                                               name="num_classe"
                                               onChange={this.handleInputChange}
                                               label="Classe"
                                           >
                                               <MenuItem value="">
                                                   <em>None</em>
                                               </MenuItem>
                                               {
                                                   data.getClasses.map(classe => (
                                                       <MenuItem value={classe.num_classe}>{classe.nom_classe}</MenuItem>
                                                   ))
                                               }

                                           </Select>
                                       </FormControl>
                                   );
                               }}

                           </Query>




                           <Button  style={{backgroundColor:'#0284AA', color:'white'}}
                                    variant="outlined"
                                    size="large"
                                    className="bouton"
                                    type="submit"
                           >Ajouter</Button>
                       </form>
                   </Box>

                )}

            </Mutation>
        );
    }
}
export default  withStyles(styles)(AjoutStagiaire);

