import React, {useState} from 'react';
import {Link} from "react-router-dom";


import {Query, useQuery} from 'react-apollo';

import Default from "../Default/Default";

import { GET_ALL_STUDENTS } from "./queries";
import Alert from "../../components/Alert/Alert";
import {Button, TextField} from "@material-ui/core";
import {Form} from "react-bootstrap";
import {forEach} from "react-bootstrap/ElementChildren";
import {useLazyQuery} from "@apollo/react-hooks";
import Sidebar from "../../components/Sidebar/Sidebar";
import Navigation from "../../components/Navigation/Navigation";

function Stagiaires (){
    const [stagiares, setStagiares] = useState([]);
    let { loading, error, data } = useQuery(GET_ALL_STUDENTS);
    if (loading){
        console.log(loading);
    }

    const handleSearch = (e) =>{
        const testTab = [];
        e.persist();
        console.log('etarget: '+ e.target.value)
        data.getAllStudents.forEach(student =>{
            if (student.nom_etudiant.toLowerCase().includes(e.target.value) ||
                student.prenom_etudiant.toLowerCase().includes(e.target.value)){
                testTab.push(student);
            }
        });
        setStagiares(testTab);
    }

    if (data && stagiares.length === 0){
        setStagiares(data.getAllStudents.slice())
    }

    return (
        <div>
            <Sidebar />
            <Navigation />
            <div className="content">

            {/* Bouton pour chercher un stagiaire */}
            <Link to="stagiaires/ajout" className="nav-link">
                <Button
                    type="submit"
                    variant="contained"
                    color="inherit"
                    style={{backgroundColor:'darkgrey', color:'black', marginLeft:20}}
                >
                    Ajouter un stagiaire
                </Button>
            </Link>

            <Form className="ml-4">
                {  <TextField onChange={handleSearch}
                              label="Rechercher"
                              name="inputSearch"
                >
                </TextField>}
            </Form>

                <div className="col-md-12">
                    <div className="content-card shadow">
                        <h3 className="content-title">Liste des stagiaires</h3>
                        <table className="table" height="50%">
                            <thead>
                            <tr>
                                <th scope="col">Nom</th>
                            </tr>
                            </thead>
                                <tbody>
                                {stagiares.map(student => (
                                    <tr key={student.num_etudiant}>
                                        <td>{student.prenom_etudiant + " " + student.nom_etudiant}</td>
                                    </tr>
                                ))}
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    );
}
export default Stagiaires;