import { gql } from "apollo-boost";


const GET_ALL_STUDENTS = gql`
   {
    getAllStudents{
        num_etudiant,
        nom_etudiant,
        prenom_etudiant
        }
    }
`;

const CREATE_STUDENT = gql`
mutation($stagiaire: EtudiantInput!) {
  createEtudiant(data: $stagiaire){num_etudiant}
}
`;

const GET_ALL_CLASSES = gql`
    {
        getClasses{
        num_classe,nom_classe
        }
    }
`

export {
    GET_ALL_STUDENTS,GET_ALL_CLASSES, CREATE_STUDENT
}